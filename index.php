<?php
$page = 'error';
$listValue = '';
$listId = 0;
$presalt = 'w7jsSu&uEA^Q*t@MBQGR';
$postsalt = '%c!TeHDJebzKq4!W^25a';
$pdo = new PDO('mysql:host=localhost;dbname=dconfirm', 'root', 'citrom');
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    //process the form
    $page = 'postprocess';
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_GET['list'])) {
        //get server http auth
        if (
            !isset($_SERVER['PHP_AUTH_USER'])
            || hash('sha256', $presalt . $_SERVER['PHP_AUTH_USER'] . $postsalt) !== 'fbbed7b5c963c83bbae8a848b99c72f907ee5c1ae1f1836331bc6f274f1bf3fa'
            || hash('sha256', $presalt . $_SERVER['PHP_AUTH_PW'] . $postsalt) !== '828ecba94b90deb75b89f24742437cd2131a317faed02df0146dee6413aecc6e'
        ) {
            header('WWW-Authenticate: Basic realm="ISJ Covasna"');
            header('HTTP/1.0 401 Unauthorized');
            echo 'Unauthorized';
            exit;
        }

        $page = 'list';
        $listValue = $_GET['list'];
        //list the query with query_id
        $listId = intval(preg_replace('/[^0-9]/', '', $listValue));

        if ($listValue === 'all') {
            //list all the queries
            $page = 'listall';
        }

        if ($listValue === 'add') {
            //input page for adding list
            $page = 'listadd';
        }
    }

    if (isset($_GET['confirm'])) {
        //confirm the query
        $page = 'confirm';
    }
} ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Confirmare Email</title>
    <link rel="stylesheet" href="/confirm/lib/bootstrap.min.css">
</head>

<body>
    <div class="container p-3">
        <?php
        //==CONFIRM PAGE=======================================================
        if ($page === 'confirm') {
            $hash = $_GET['confirm'];
            $pdo->query("UPDATE `email` SET `confirmed` = 1 WHERE `email`.`hashed` = '" . $hash . "' AND `email`.`confirmed`=0;");
        ?>
            <div class="card " style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Adresă confirmată</h5>
                    <h6 class="card-subtitle mb-2 text-body-secondary">Email</h6>
                    <p class="card-text">Adresa Dvs. de email a fost confirmată în bazele de date ale ISJ Covasna. Acesta va fi utilizată strict în scopul de a vă da acces la platformele Ministerului Educației și pentru comunicări oficiale.</p>
                </div>
            </div>

        <?php
        }
        //==LISTALL PAGE=======================================================
        elseif ($page === 'listall') {
            $result = $pdo->query('SELECT l.id, l.name, count(*) as Total, sum(e.confirmed) as Confirmat FROM `email_on_list` el left join `list` l on el.list_id=l.id left join `email` e on el.email_hashed=e.hashed group by l.id,l.name');
        ?>
            <a class="btn btn-primary" href="/confirm/list/add">Adaugă listă</a>
            <h3>Liste Existente</h3>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Nume</th>
                        <th scope="col">Total</th>
                        <th scope="col">Confirmat</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($result as $row) {
                        echo '<tr>';
                        echo '<th scope="row">' . $row['id'] . '</th>';
                        echo '<td><a href="/confirm/list/' . $row['id'] . '">' . $row['name'] . '</a></td>';
                        echo '<td>' . $row['Total'] . '</td>';
                        echo '<td>' . $row['Confirmat'];
                        if ($row['Total'] == $row['Confirmat']) {
                            echo '<span class="badge text-success">✓</span>';
                        }
                        echo '</td>';
                        echo '</tr>';
                    }
                    ?>
                </tbody>
            </table>
        <?php
        }
        //==LISTADD PAGE=======================================================
        elseif ($page === 'listadd') {
        ?>
            <form class="mx-auto" style="width:30em;" action="/confirm/list/add" method="POST">
                <div class="mb-3">
                    <label for="listname" class="form-label">Nume listă</label>
                    <input type="text" class="form-control" id="listname" name="listname">
                </div>
                <div class="mb-3">
                    <label for="emaillist" class="form-label">Adrese email</label>
                    <textarea class="form-control" name="emaillist" id="emaillist" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        <?php
        }
        //==LIST PAGE==========================================================
        elseif ($page === 'list') {

            // server protocol
            $protocol = empty($_SERVER['HTTPS']) ? 'http' : 'https';
            // domain name
            $domain = $_SERVER['SERVER_NAME'];
            $url = "$protocol://$domain";

            $result = $pdo->query("SELECT * FROM `list` WHERE `id`=" . $listId . ";");
            $listName = $result->fetch()['name'];
            echo '<h3>' . $listName . '</h3>';
            $result2 = $pdo->query("SELECT 
            e.email,
            e.hashed, 
            e.confirmed 
            FROM 
            `email_on_list` el 
            left join `email` e 
            on el.email_hashed=e.hashed 
            WHERE el.list_id=" . $listId . ";");
        ?>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Email</th>
                        <th scope="col">Link</th>
                        <th scope="col">Confirmat</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($result2 as $row) {
                        echo '<tr>';
                        echo '<th scope="row">' . $row['email'] . '</th>';

                        echo '<td><a href="/confirm/' . $row['hashed'] . '">' . $url . "/confirm" . "/" . $row['hashed'] . '</a></td>';

                        if ($row['confirmed'] == 1) {
                            echo '<td><span class="badge text-success">✓</span>';
                        } else {
                            echo '<td><span class="badge text-danger">x</span></td>';
                        }
                        echo '</tr>';
                    }
                    ?>
                </tbody>
            </table>
        <?php
        }
        //==POSTPROCESS PAGE===================================================
        elseif ($page === 'postprocess') {
            $listname = $_POST['listname'];
            $emaillist = $_POST['emaillist'];
            $emaillist = str_replace("\r", "", $emaillist);
            $emailarray = explode("\n", $emaillist);
            $emailarray = array_map('trim', $emailarray);

            $emailHashedArray = array_map(function ($email) {
                global $presalt, $postsalt;
                return array($email, hash('sha256', $presalt . $email . $postsalt));
            }, $emailarray);

            $queryString = "INSERT INTO `list` (`name`) VALUES ('" . $listname . "');";
            $pdo->query($queryString);
            $newListId = $pdo->lastInsertId();

            $queryString = "INSERT INTO `email` (`email`, `hashed`) VALUES ";
            $emailOnListQueryString = "INSERT INTO `email_on_list` (`list_id`, `email_hashed`) VALUES ";

            foreach ($emailHashedArray as $email) {
                $queryString .= "('" . $email[0] . "','" . $email[1] . "'),";
                $emailOnListQueryString .= "('" . $newListId . "','" . $email[1] . "'),";
            }

            $queryString = substr($queryString, 0, -1) . " ON DUPLICATE KEY UPDATE `email`=`email`;";
            $pdo->query($queryString);

            $emailOnListQueryString = substr($emailOnListQueryString, 0, -1) . ";";
            $pdo->query($emailOnListQueryString);
        ?>
            <div class="card " style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Listă creată</h5>
                    <h6 class="card-subtitle mb-2 text-body-secondary">Email</h6>
                    <p class="card-text">A fost creată <a href="/confirm/list/<?php echo $newListId; ?>">Lista cu ID <?php echo $newListId; ?></a></p>
                </div>
            </div>
        <?php
        }
        ?>

    </div>
</body>

</html>