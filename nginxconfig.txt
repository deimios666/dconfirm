location /confirm/list {
    rewrite ^/confirm/list/(.*)?$ /confirm/index.php?list=$1 last;
    rewrite ^/confirm/list$ /confirm/index.php?list=all last;
    try_files $uri $uri/ /index.php;
}

location /confirm/lib {

}

location /confirm {
    rewrite ^/confirm/(.*)?$ /confirm/index.php?confirm=$1 last;
    index  index.php index.html index.htm;
}
